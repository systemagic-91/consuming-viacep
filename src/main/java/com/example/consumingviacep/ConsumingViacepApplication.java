package com.example.consumingviacep;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
public class ConsumingViacepApplication {

	private static final Logger log = LoggerFactory.getLogger(ConsumingViacepApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ConsumingViacepApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder){
		return builder.build();
	}

	@Bean
	public CommandLineRunner run(RestTemplate restTemplate) throws Exception{
		return args -> {
			ViaCEP viaCEP = restTemplate.getForObject(
				"https://viacep.com.br/ws/32110040/json/", 
				ViaCEP.class
			);
			log.info(viaCEP.toString());
		};
	}

}
